package com.example.quizapp.ui.quiz

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.quizapp.R
import com.example.quizapp.model.Question
import com.example.quizapp.ui.result.ResultActivity
import com.example.quizapp.util.Constans
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import kotlinx.android.synthetic.main.activity_quiz.*

class QuizActivity : AppCompatActivity() {

    private lateinit var viewModel1: QuizViewModel
    private var questionList: List<Question> = arrayListOf()
    private var qindex: Int = 0
    private var score: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        /** database hazirladik artik livedatayi gozemleyebiliriz(ui olarak) */

        viewModel1 = ViewModelProviders.of(this).get(QuizViewModel::class.java)
        viewModel1.allOuestions.observe(this, Observer {
//            nextQuestionCheck(qindex+1)
            if (it.isNotEmpty()) {
                /** lsite bos degilse  */
                questionList = it

                setViews()
                /** ilk sorumuzu gosterdik */
                btnNext.setOnClickListener {
                    val answer = findViewById<Chip>(optionChipGroup.checkedChipId)

                    if (answer != null) {
                        /** chip grouptan sectigimiz item tutacak */
                        optionChipGroup.clearCheck()
                        /** next bastiktan sonra secilen chip temizlensinki dgier soruyu etkilemesin */


                        /** secilen  cevabin dogru olup olmadigini controlu */
                        checkAnswer(answer)

                        /** sonraki soruya gecmek icin index 1 arttirdik */
                        qindex++

                        /** liste tasma hatasi (array index bound hatasi) almayalim*/
                        if (qindex < questionList.size) {
                            setViews()
                        } else {
                            val intent = Intent(this, ResultActivity::class.java)
                            intent.putExtra(Constans.EXTRA_RESULT, score)
                            intent.putExtra(Constans.EXTRA_LIST_SIZE, questionList.size)
                            startActivity(intent)
                            finish()
                            /** finish yazamizin nedeni geri basıldıgında bu sorulara gitmesin */
                        }
                    } else {
                        Toast.makeText(this, "Lutfen Bir Cevap İşaretleyin!!!", Toast.LENGTH_SHORT)
                            .show()
                    }

                }

            }
        })


    }

//   private fun nextQuestionCheck(temp:Int):Boolean{
//        if(temp==questionList.size){
//            Toast.makeText(this,"Tebrikler Tum Sorulari Cevapladiniz!!!",Toast.LENGTH_LONG).show()
//           // (findViewById<Button>(R.id.btnNext)).setVisibility(View.GONE)
//            return true
//        }
//       return false
//    }
    private fun setViews() {
        txtQuestion.text = questionList[qindex].question
        optionChipA.text = questionList[qindex].optionA
        optionChipB.text = questionList[qindex].optionB
        optionChipC.text = questionList[qindex].optionC

    }

    private fun checkAnswer(answer: Chip) {
        /** verilen cevabin dogru oplup olmadigini kontrol eden method */

        if (questionList[qindex].answer == answer.text) {
            Toast.makeText(this, "Dogru Cevap", Toast.LENGTH_SHORT).show()
            score++
        } else {
            Toast.makeText(
                this,
                "Yanlıs Cevap \n Cevap=${questionList[qindex].answer}",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

}

