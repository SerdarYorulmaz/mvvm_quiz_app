package com.example.quizapp.ui.start

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.quizapp.R
import com.example.quizapp.ui.add.AddQuestionActivity
import com.example.quizapp.ui.quiz.QuizActivity
import kotlinx.android.synthetic.main.activity_start.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        btnEntry.setOnClickListener {
            startActivity(Intent(this, QuizActivity::class.java))
            finish()
        }
        //  Toast.makeText(this,"Test Denem", Toast.LENGTH_SHORT).show()

        btnAddQuestion.setOnClickListener {
            startActivity(Intent(this,AddQuestionActivity::class.java))
        }

    }
}
