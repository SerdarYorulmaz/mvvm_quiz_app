package com.example.quizapp.ui.quiz

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.quizapp.data.QuestionDao
import com.example.quizapp.data.QuestionDb
import com.example.quizapp.model.Question

/** QuizRepository gorevi, bizim olusturudumuz room database ile iletişim halinde  aldigi degerileri viewmodel gonderecek viewmodel da da live data ile verileri gozlemle
 * yecegiz. boylelikle ui verilerin degisiminden haberdar olacak. */

class QuizRepository(context: Context) {

    /** not lazy yerine init blogundada yapilabilirdi lakin biz tek satirda hem degiskene atadik  hemde init in gorevini yapmis olduk */
    private val db by lazy {
        QuestionDb.getInstance(context)
    }
    private val dao: QuestionDao by lazy {
        db.questionDao()
    }

    /** aagidaki method livedata dondurecek veritabaninda aldigi veriler i viewmodel a gonderecek.verler liste halinde dondugu icin
     * Question turunden oldugu icin   LiveData<List<Question>> türünden yazdik    */

    /** veritabanin icinde tum soruları getir  burdaki methodu calistiracak olan view model dir.NOT: CONTEXT en haberdar olan viewmodel a AndroidViewModel() denir */
    fun getAllQuestions():LiveData<List<Question>> =
            dao.getQuestions()
}