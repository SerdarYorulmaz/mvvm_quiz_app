package com.example.quizapp.ui.crud

import android.content.Context
import android.os.AsyncTask
import com.example.quizapp.data.QuestionDao
import com.example.quizapp.data.QuestionDb
import com.example.quizapp.model.Question

class CrudQuestionRepository(context: Context) {

    /** database ve dao islemleri icin onlari cagirdik */

    private  val db:QuestionDb by lazy { QuestionDb.getInstance(context) }

    private  val dao:QuestionDao by lazy { db.questionDao() }

    fun update(question: Question){
        val updateQuestionAsyncTask=UpdateQuestionAsyncTask(dao).execute(question)
    }

    private class UpdateQuestionAsyncTask(val dao:QuestionDao):AsyncTask<Question,Void,Void>(){
        override fun doInBackground(vararg params: Question?): Void? {
            params[0]?.let { dao.updateQuestion(it) }
            return null
        }

    }
}