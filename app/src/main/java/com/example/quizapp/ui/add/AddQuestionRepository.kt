package com.example.quizapp.ui.add

import android.content.Context
import android.os.AsyncTask
import com.example.quizapp.data.QuestionDao
import com.example.quizapp.data.QuestionDb
import com.example.quizapp.model.Question

class AddQuestionRepository (context:Context){

    /** yine database ve  dao aldik */
    private val db:QuestionDb by lazy { QuestionDb.getInstance(context) }
    private val dao:QuestionDao by lazy { db.questionDao() }

    /** soru ekleme methdou olusturuyoruz */

    fun insertQuestion(question:Question){
        /** dao InsertAsyncTask consteru oldugunda vermeliyiz methodda parametre olarak question istiyor onu verdik */
        InsertAsyncTask(dao).execute(question)
    }

    /** soru ekleme kismi arka planda(isci threadler) yapacak cunku ui kitlememiz icin  */

    private class InsertAsyncTask(val dao:QuestionDao): AsyncTask<Question,Void,Void>(){
        override fun doInBackground(vararg params: Question?): Void? {
          /** ilk parametreyi alacagimizidan params[0] dedik */
            params[0]?.let { dao.addQuestion(it) }
            return  null
        }

    }

}