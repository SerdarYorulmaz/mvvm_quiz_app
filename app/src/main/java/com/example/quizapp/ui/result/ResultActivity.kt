package com.example.quizapp.ui.result

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.quizapp.R
import com.example.quizapp.ui.add.AddQuestionActivity
import com.example.quizapp.ui.quiz.QuizActivity
import com.example.quizapp.ui.start.StartActivity
import com.example.quizapp.util.Constans
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        intent.extras.let {
            var result=intent.extras?.getInt(Constans.EXTRA_RESULT)
            var listSize=intent.extras?.getInt(Constans.EXTRA_LIST_SIZE)
            txtResult.setText("${listSize} sorudan ${result} tanesini doğru bildiniz")

            btnQuestionAgain.setOnClickListener {
                startActivity(Intent(this, QuizActivity::class.java))
                finish()
            }
            btnMainScreen.setOnClickListener {
                startActivity(Intent(this, StartActivity::class.java))
                finish()
            }
        }
    }
}
