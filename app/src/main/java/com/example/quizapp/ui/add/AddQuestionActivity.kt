package com.example.quizapp.ui.add

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.quizapp.R
import com.example.quizapp.model.Question
import kotlinx.android.synthetic.main.activity_add_question.*

class AddQuestionActivity : AppCompatActivity() {

    private lateinit var viewModel: AddQuestionViewModel
    private lateinit var tempAnswer: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_question)


        /** view model tanimladik */
        viewModel = ViewModelProviders.of(this).get(AddQuestionViewModel::class.java)

        prepareSpinner()
        btnQuestionSave.setOnClickListener {


            /** edit text deki degerleri aliyoruz */
            val question = txtEditQuestion.text.toString()
            val optionA = txtEditAnswerA.text.toString()
            val optionB = txtEditAnswerB.text.toString()
            val optionC = txtEditAnswerC.text.toString()

            if ((question.trim().isBlank()) || (optionA.trim().isBlank()) || (optionB.trim().isBlank()) || (optionC.trim().isBlank())) {

                Toast.makeText(this, "Lutfen Gerekli Yerleri Doldurunuz!!", Toast.LENGTH_SHORT)
                    .show()


            } else {

                /** Ekleme işlemini yapiyoruz */
                viewModel.insert(
                    Question(

                        question = question,
                        optionA = optionA,
                        optionB = optionB,
                        optionC = optionC,
                        answer = if (tempAnswer == "A") optionA else if (tempAnswer == "B") optionB else optionC
                    )
                )

                Toast.makeText(this, "Ekleme Başarılı", Toast.LENGTH_SHORT).show()
                textClear()
            }

        }

    }

    private fun textClear(){
        txtEditQuestion.setText("")
        txtEditAnswerA.setText("")
        txtEditAnswerB.setText("")
        txtEditAnswerC.setText("")
        prepareSpinner()
    }

    private fun prepareSpinner() {
        /** listemizi spinner ekledik(array adapter sayesinde) */
        var optList = mutableListOf("A", "B", "C")
        var adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, optList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spnTrueAnswer.adapter = adapter

        /**  spinner dan secme islemi*/
        spnTrueAnswer.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO() // secilmemesi durumumu bu methodla isimiz yok
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {

                tempAnswer = parent?.getItemAtPosition(position).toString()

            }

        }
    }
}
