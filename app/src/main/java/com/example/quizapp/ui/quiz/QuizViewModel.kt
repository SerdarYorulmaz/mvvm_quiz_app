package com.example.quizapp.ui.quiz

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.quizapp.model.Question

/** neden sadece viewmodel() yerine AndroidViewModel() kullanidik bize context lazim.AndroidViewmodel() bize veriyor. */

class QuizViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: QuizRepository by lazy {
        QuizRepository(application)
    }
    /**  repository aldik (init seklin de de yapabilirdik )*/

    val allOuestions: LiveData<List<Question>> by lazy {
        repository.getAllQuestions()
    }  /**  repositorydeki  getAllQuestions methodunu calistimis olduk*/

}