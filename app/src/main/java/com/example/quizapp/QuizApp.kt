package com.example.quizapp

import android.app.Application
import com.facebook.stetho.Stetho

class QuizApp:Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this) /** sqlLite olusturuduguuz veritabanini choreme gormemizi saglayan kutuphane */
    }
}