package com.example.quizapp.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.quizapp.model.Question

@Dao
interface QuestionDao {
    /** veritabanin yapmak istedigimiz select,updatedelete gibi  özel queryler yazabiliriz.s  */

    @Query("SELECT * FROM questions")
    fun getQuestions():LiveData<List<Question>>

    @Insert(onConflict = OnConflictStrategy.REPLACE) /** soru ekleme ,onConflict = OnConflictStrategy.REPLACE--> var olan veri tekrar eklenmek istendiginde onun ile degistir.*/
    fun addQuestion(question: Question)

    @Query("Delete from questions")
    fun deleteAll()

    @Delete
    fun deleteQuestion(question: Question)

    @Update
    fun updateQuestion(question: Question)
}