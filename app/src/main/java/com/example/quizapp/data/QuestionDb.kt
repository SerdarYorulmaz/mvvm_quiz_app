package com.example.quizapp.data

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.quizapp.model.Question

@Database(entities = arrayOf(Question::class), version = 1)
abstract class QuestionDb : RoomDatabase() {
    abstract fun questionDao(): QuestionDao

    companion object {
        /** static olrak tanimliyoruz */

        /** @Volatile  gecici denemek  */

        @Volatile
        var INSTANCE: QuestionDb? = null

        @Synchronized
        fun getInstance(context: Context): QuestionDb {

            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    /** burda uygulamanin contexsini aliyoruz */
                    QuestionDb::class.java,
                    "question_db"
                )
                    .addCallback(roomDbCallback)
                    /** veritabani ilk kez olusturuldugunda  roomcallback ile veriler biz hazir gelecek */
                    .build()
            }

            return INSTANCE as QuestionDb
        }

        /** roomDbCallback her database olusturuldugunda calistiralacak ve onCreate  methodun bize hazir veriler saglayacak.*/
        private val roomDbCallback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)

                /** verileri backgroun therad olustracagimizdan asyntask olusturacagiz (NOT: android de ui(main) le background(isci) threadler var)
                 *
                 * peki neden background thread kullaniyoruz bas islemler arka plan yapılması gerekiyor örnek biz veritabanina ekle yapmak istiyorsak
                 *  o anda ekranı kilitlemek istemiyorsak background(arka palanda) yapmamiz gerekiyor.Background  yapmak icinde threadler kullnmamiz gerekiyor.
                 *
                 *
                 * PopulateAsyncTask --> arka planda calisacak(background threadde gerceklescek)
                 * */

                PopulateAsyncTask(INSTANCE).execute()
            }
        }
                class PopulateAsyncTask(private val db: QuestionDb?) :
                    AsyncTask<Void, Void, Void>() {

                    private val dao: QuestionDao? by lazy {
                        db?.questionDao()
                    }

                    override fun doInBackground(vararg params: Void?): Void? {
                        var question = Question(
                            question = "Android yazdigimiz programlardan biridir?",
                            optionA = "JAVA",
                            optionB = "KOTLIN",
                            optionC = "HEPSI",

                            answer = "HEPSI"
                        )
                        dao?.addQuestion(question)

                        question=Question(
                            question = "Turkiye nin baskenti?",
                            optionA = "Ankara",
                            optionB = "Istanbul",
                            optionC = "Izmir",

                            answer = "Ankara"
                        )
                        dao?.addQuestion(question)

                        question=Question(
                            question = "Turkiye nin kac bolgesi var?",
                            optionA = "5",
                            optionB = "7",
                            optionC = "9",

                            answer = "7"
                        )
                        dao?.addQuestion(question)


                        return null
                    }
                }




    }
}